package com.example.jyotsna_thaninki.expensesapp;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jyothsna_thaninki on 21/2/17.
 */
public class DataManager {
    private static final String URL = "http://192.168.91.131:3011/";
    private static DataManager dataManager;

   private Retrofit retrofit;

    private DataManager() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        // add your other interceptors …
        // add logging as last interceptor
        httpClient.addInterceptor(logging);  // <-- this is the important line!

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit = new Retrofit.Builder().baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient.build())
                .build();
    }

    public static DataManager getDataManager() {
        if(dataManager == null) {
            dataManager = new DataManager();
        }
        return dataManager;
    }

    public void userLogin(Callback<UserDetails> cb, String mobile_number, String user_password) {

        UserDetailsAPI apiService = retrofit.create(UserDetailsAPI.class);
        Call<UserDetails> call = apiService.userLogin(mobile_number, user_password);
        call.enqueue(cb);

    }

    public void userSignup(Callback<UserDetails> cb, String user_name, String mobile_number, String user_password) {

        UserDetailsAPI apiService = retrofit.create(UserDetailsAPI.class);
        Call<UserDetails> call = apiService.userSignup(user_name, mobile_number, user_password);
        call.enqueue(cb);
    }

    public void userCheckEmail(Callback<UserDetails> cb, String email_or_mobile) {

        UserDetailsAPI apiService = retrofit.create(UserDetailsAPI.class);
        Call<UserDetails> call = apiService.userCheckEmail(email_or_mobile);
        call.enqueue(cb);
    }
    public void userResetPassword(Callback<UserDetails> cb, String email_or_mobile, UserDetails body1) {

        UserDetailsAPI apiService = retrofit.create(UserDetailsAPI.class);
        Call<UserDetails> call = apiService.userResetPassword(email_or_mobile,body1);
        call.enqueue(cb);
    }
    public void insertExpense(Callback<ExpenselistPOJO> cb,String mobile_number, String expense_name, Date purchase_date, String purpose, String amount, String paid_by) {

        UserDetailsAPI apiService = retrofit.create(UserDetailsAPI.class);
        Call<ExpenselistPOJO> call = apiService.insertExpense(mobile_number,expense_name, purchase_date, purpose,amount,paid_by);
        call.enqueue(cb);
    }
    public void getExpense(Callback<ArrayList<ExpenselistPOJO>> cb, String expense_name) {

        UserDetailsAPI apiService = retrofit.create(UserDetailsAPI.class);
        Call<ArrayList<ExpenselistPOJO>> call = apiService.getExpense(expense_name);
        call.enqueue(cb);
    }

    public void getExpenseList(Callback<ArrayList<ExpenselistPOJO>> cb,String mobile_number) {

        UserDetailsAPI apiService = retrofit.create(UserDetailsAPI.class);
        Call<ArrayList<ExpenselistPOJO>> call = apiService.getExpenseList(mobile_number);
        call.enqueue(cb);
    }

    public void updateExpense(Callback<UserDetails> cb, String expense_name, ExpenselistPOJO body) {

        UserDetailsAPI apiService = retrofit.create(UserDetailsAPI.class);
        Call<UserDetails> call = apiService.updateExpense(expense_name,body);
        call.enqueue(cb);
    }
    public void postImage(Callback<ResponseBody> cb,List<MultipartBody.Part> body,String expense_name) {

        UserDetailsAPI apiService = retrofit.create(UserDetailsAPI.class);
        Call<ResponseBody> call = apiService.postImage(body,expense_name);
        call.enqueue(cb);
    }

    public void getImageID(Callback<ArrayList<String>> cb, String expense_name) {

        UserDetailsAPI apiService = retrofit.create(UserDetailsAPI.class);
        Call<ArrayList<String>> call = apiService.getImageID(expense_name);
        call.enqueue(cb);
    }
    public void deleteReceipt(Callback<ResponseBody> cb, String receipt_id) {

        UserDetailsAPI apiService = retrofit.create(UserDetailsAPI.class);
        Call<ResponseBody> call = apiService.deleteReceipt(receipt_id);
        call.enqueue(cb);
    }

}










