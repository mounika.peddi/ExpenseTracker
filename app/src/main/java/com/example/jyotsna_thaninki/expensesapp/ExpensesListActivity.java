package com.example.jyotsna_thaninki.expensesapp;


import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ExpensesListActivity extends ListActivity {
    private FloatingActionButton fab;
    String mobile_number;
    Intent intent;
    ListView listview_settings = null;
    CustomListAdapter adapter;
    ArrayList<ExpenselistPOJO> user;
    SharedPreferences preferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expenses_list);
        listview_settings = (ListView) findViewById(android.R.id.list);
        listview_settings.setBackgroundColor(Color.WHITE);
        preferences=getSharedPreferences("myprefs", Activity.MODE_PRIVATE);

        mobile_number=  preferences.getString("mobile_number",null);

        serviceInit();

        listview_settings.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                intent = new Intent(getApplicationContext(), ExpensesListDetailsActivity.class);
                intent.putExtra("expense_name", user.get(i).getExpense_name());
                startActivity(intent);



            }


        });
        fab = (FloatingActionButton) findViewById(R.id.floating_action_button_add);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                intent = new Intent(ExpensesListActivity.this, ItemDetails.class);

                startActivity(intent);


            }
        });
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return (keyCode == KeyEvent.KEYCODE_BACK || super.onKeyDown(keyCode, event));
    }

    public void serviceInit() {
        DataManager dataManager = DataManager.getDataManager();

        dataManager.getExpenseList(new Callback<ArrayList<ExpenselistPOJO>>() {
            @Override
            public void onResponse(Call<ArrayList<ExpenselistPOJO>> call, Response<ArrayList<ExpenselistPOJO>> response) {

                    if (response.isSuccessful()) {
                        Toast.makeText(getApplicationContext(), " Valid User...", Toast.LENGTH_LONG).show();

                        user = response.body();
                        adapter = new CustomListAdapter(ExpensesListActivity.this, user);
                        listview_settings.setAdapter(adapter);

                    }
            }

            @Override
            public void onFailure(Call<ArrayList<ExpenselistPOJO>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), " Invalid User...", Toast.LENGTH_LONG).show();
            }

        },mobile_number);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_logout, menu);
        return  true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
               SharedPreferences.Editor editor = preferences.edit();
                editor = editor.clear();
                editor.commit();
                Intent intent=new Intent(ExpensesListActivity.this,LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}