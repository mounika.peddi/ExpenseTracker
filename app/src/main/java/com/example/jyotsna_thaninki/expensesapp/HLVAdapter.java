package com.example.jyotsna_thaninki.expensesapp;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class HLVAdapter extends RecyclerView.Adapter<HLVAdapter.ViewHolder> {

    ArrayList<String>  alName;
    Context context;
    public HLVAdapter(Context context, ArrayList<String>  alName) {
        super();

        this.alName = alName;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.imagelist_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {

       viewHolder.tvSpecies.setText(alName.get(i).substring(alName.get(i).lastIndexOf("/")+1));
        String image_name=alName.get(i).substring(alName.get(i).lastIndexOf("/")+1);
        viewHolder.tvSpecies.setText(image_name);

        final Bitmap bitmap= BitmapFactory.decodeFile(alName.get(i));
        viewHolder.selectedimage.setImageBitmap(bitmap);
        viewHolder.selectedimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView img = new ImageView(context);
                    img.setImageBitmap(bitmap);
                    img.getAdjustViewBounds();
                    Dialog settingsDialog = new Dialog(context);
                    settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                    settingsDialog.setContentView(img);
                    settingsDialog.show();

            }
        });
        viewHolder.mRemoveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get the clicked item label
                String itemLabel = alName.get(i);

                // Remove the item on remove/button click
                alName.remove(i);
                // Show the removed item label
                Toast.makeText(context,"Removed : " + itemLabel,Toast.LENGTH_SHORT).show();
                /*
                    public final void notifyItemRemoved (int position)
                        Notify any registered observers that the item previously located at position
                        has been removed from the data set. The items previously located at and
                        after position may now be found at oldPosition - 1.

                        This is a structural change event. Representations of other existing items
                        in the data set are still considered up to date and will not be rebound,
                        though their positions may be altered.

                    Parameters
                        position : Position of the item that has now been removed
                */
                notifyItemRemoved(i);

                /*
                    public final void notifyItemRangeChanged (int positionStart, int itemCount)
                        Notify any registered observers that the itemCount items starting at
                        position positionStart have changed. Equivalent to calling
                        notifyItemRangeChanged(position, itemCount, null);.

                        This is an item change event, not a structural change event. It indicates
                        that any reflection of the data in the given position range is out of date
                        and should be updated. The items in the given range retain the same identity.

                    Parameters
                        positionStart : Position of the first item that has changed
                        itemCount : Number of items that have changed
                */
                notifyItemRangeChanged(i,alName.size());


            }
        });

    }

    @Override
    public int getItemCount() {
        return alName.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public ImageView selectedimage;
        public TextView tvSpecies;
        ImageButton mRemoveButton;
        private ItemClickListener clickListener;
        public ViewHolder(View itemView) {
            super(itemView);
            selectedimage = (ImageView) itemView.findViewById(R.id.selected_image);
           tvSpecies = (TextView) itemView.findViewById(R.id.image_name);
            mRemoveButton = (ImageButton) itemView.findViewById(R.id.ib_remove);
        }

    }

}
