package com.example.jyotsna_thaninki.expensesapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;



public class LaunchActivity extends Activity {

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    Intent launchIntent = new Intent();
    Class<?> launchActivity;

    launchActivity = getLaunchClass();
    launchIntent.setClass(getApplicationContext(), launchActivity);
    startActivity(launchIntent);

    finish();
  }

  private Class<?> getLaunchClass() {
    SharedPreferences preferences=getSharedPreferences("myprefs", Activity.MODE_PRIVATE);
    boolean isLoggedIn=preferences.getBoolean("isLoggedIn",false);
    if (isLoggedIn) {
      return ExpensesListActivity.class;
    } else {
      return LoginActivity.class;
    }
  }
}
