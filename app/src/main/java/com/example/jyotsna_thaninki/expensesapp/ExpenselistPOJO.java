package com.example.jyotsna_thaninki.expensesapp;

import java.util.Date;

/**
 * Created by mounikapeddi on 29/5/17.
 */

public class ExpenselistPOJO {
    String expense_name,purpose,amount,paid_by;
    Date purchase_date;

    public ExpenselistPOJO(){

    }
    public Date getPurchase_date() {
        return purchase_date;
    }

    public void setPurchase_date(Date purchase_date) {
        this.purchase_date = purchase_date;
    }



    public String getExpense_name() {
        return expense_name;
    }

    public void setExpense_name(String expense_name) {
        this.expense_name = expense_name;
    }


    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPaid_by() {
        return paid_by;
    }

    public void setPaid_by(String paid_by) {
        this.paid_by = paid_by;
    }
}
