package com.example.jyotsna_thaninki.expensesapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReceiptAttach extends AppCompatActivity{
    private static final int PICK_IMAGE_ID = 1;
    private final int PICK_IMAGE_MULTIPLE = 1;
    Button  upload;
    String[] imagesPath;
    private ArrayList<String> imagesPathList = new ArrayList<>();
    private ArrayList<String> imagesPathListfinal = new ArrayList<>();
    String expense_name;
    RecyclerView mRecyclerView;
    HLVAdapter mAdapter;
    List<MultipartBody.Part> parts = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_receipt_attach_fragment);


        upload=(Button)findViewById(R.id.upload_button);

        // Calling the RecyclerView
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        expense_name = getIntent().getStringExtra("expense_name");
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              serviceInit();

                Intent intent=new Intent(ReceiptAttach.this,ExpensesListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(intent);


            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return  true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.attach_file:
                Permissions permissions=new Permissions();
                if (permissions.checkPermissionREAD_EXTERNAL_STORAGE(this)) {
                    Toast.makeText(this, "select image", Toast.LENGTH_SHORT).show();
                    pickImages();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == PICK_IMAGE_MULTIPLE) {
                imagesPathList = new ArrayList<>();
          imagesPath = data.getStringExtra("data").split("\\|");

            }

        for (String anImagesPath : imagesPath) {
            imagesPathList.add(anImagesPath);
        }
        imagesPathListfinal.addAll(imagesPathList);

        mAdapter = new HLVAdapter(this, imagesPathListfinal);
        mRecyclerView.setAdapter(mAdapter);
        try {
            FileOutputStream fileOutputStream = this.openFileOutput("output.txt", Context.MODE_PRIVATE);
            ObjectOutputStream out = new ObjectOutputStream(fileOutputStream);
            out.writeObject(imagesPathListfinal);
            out.close();
            fileOutputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }


    }
    }
    private void pickImages() {
        Intent chooseImageIntent = new Intent(this, CustomPhotoGalleryActivity.class);
        startActivityForResult(chooseImageIntent, PICK_IMAGE_ID);
    }
    public void serviceInit() {

        DataManager dataManager = DataManager.getDataManager();
       for(int i=0;i<imagesPathListfinal.size();i++) {


            File file = new File(imagesPathListfinal.get(i));
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part body = MultipartBody.Part.createFormData("image", file.getName(), reqFile);
            parts.add(body);
     }


        dataManager.postImage(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), " Images uploaded", Toast.LENGTH_LONG).show();
                }
            else {
                    Toast.makeText(getApplicationContext(), " Error.....   ", Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }

        }, parts,expense_name);

    }

}
