package com.example.jyotsna_thaninki.expensesapp;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ExpensesListDetailsActivity extends AppCompatActivity {
    EditText date,amount,paid_by,expense_name,purpose;
    TextView textView;
    SpannableString content;
    Button save;
    String s;
    ArrayList<ExpenselistPOJO> user;
    String list_name;
    DataManager dataManager = DataManager.getDataManager();
   Date myDate;
    String datetime;
    SimpleDateFormat df;
    public ExpensesListDetailsActivity() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_expenses_listdetails);
        purpose = (EditText) findViewById(R.id.purpose_ed);

       df = new SimpleDateFormat("dd-MM-yyyy");

        date = (EditText) findViewById(R.id.date_ed_txt);
        amount = (EditText) findViewById(R.id.amount_ed_txt);
        paid_by = (EditText) findViewById(R.id.paidby_ed_txt);
        expense_name = (EditText) findViewById(R.id.expense_name_ed_txt);
        save = (Button) findViewById(R.id.save);
       list_name = getIntent().getStringExtra("expense_name");

        getSupportActionBar().setTitle(list_name);

        serviceInit();

       save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ExpenselistPOJO updateExpense=new ExpenselistPOJO();

                String dtStart = date.getText().toString();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                try {
             myDate = format.parse(dtStart);
                    System.out.println(date);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                updateExpense.setPurchase_date(myDate);
                updateExpense.setAmount(amount.getText().toString());
                updateExpense.setPurpose(purpose.getText().toString());
                updateExpense.setPaid_by(paid_by.getText().toString());
                dataManager.updateExpense(new Callback<UserDetails>() {
                    @Override
                    public void onResponse(Call<UserDetails> call, Response<UserDetails> response) {
                      //User
                        UserDetails user= response.body();
                        if (!(user.getError())) {

                            Toast.makeText(getApplicationContext(), "expense updated successfully", Toast.LENGTH_LONG).show();
                        }
                        else{
                            Toast.makeText(getApplicationContext(), "error...", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<UserDetails> call, Throwable t)
                    {
                        Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
                    }
                },list_name,updateExpense);

                Intent intent = new Intent(ExpensesListDetailsActivity.this, ExpensesListActivity.class);
                startActivity(intent);
            }
        });


        textView = (TextView) findViewById(R.id.show_receipts_link);
        content = new SpannableString("Show Receipts");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        textView.setText(content);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), FreeActivity.class);
                intent.putExtra("expense_name",list_name);
                startActivity(intent);


            }

        });

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY, 23);
                c.set(Calendar.MINUTE, 59);
                c.set(Calendar.SECOND, 59);
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                c.add(Calendar.DATE, 0);


                DatePickerDialog datePickerDialog = new DatePickerDialog(ExpensesListDetailsActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());

                datePickerDialog.show();
            }
        });




    }
    public void serviceInit() {

        dataManager.getExpense(new Callback<ArrayList<ExpenselistPOJO>>() {
            @Override
            public void onResponse(Call<ArrayList<ExpenselistPOJO>> call, Response<ArrayList<ExpenselistPOJO>> response) {

            if (response.isSuccessful()) {
                user = response.body();
                    Toast.makeText(getApplicationContext(), " Valid User...", Toast.LENGTH_LONG).show();

                Date today = user.get(0).getPurchase_date();

                SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

                datetime = dateformat.format(today);
                    expense_name.setText(user.get(0).getExpense_name());
                    date.setText(datetime);
                    purpose.setText(user.get(0).getPurpose());
                    amount.setText(user.get(0).getAmount());
                    paid_by.setText(user.get(0).getPaid_by());

             }

            }

            @Override
            public void onFailure(Call<ArrayList<ExpenselistPOJO>> call, Throwable t) {

            }

        }, list_name);



    }
}
