package com.example.jyotsna_thaninki.expensesapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FreeActivity extends AppCompatActivity {
    private static final int PICK_IMAGE_ID = 1;
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    private ArrayList<String> imagesPathList = new ArrayList<>();
    private final int PICK_IMAGE_MULTIPLE = 1;
    String[] imagesPath;
    int imagelist_size;
    String list_name;
    ViewPager viewPager;
    MyViewPagerAdapter myViewPagerAdapter;
    ArrayList<String>  user;
    List<MultipartBody.Part> parts = new ArrayList<>();
    DataManager dataManager = DataManager.getDataManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_receipts);

        viewPager = (ViewPager) findViewById(R.id.vp_slider);

        list_name = getIntent().getStringExtra("expense_name");

   ActionBar actionBar=getSupportActionBar();

        //   actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        serviceInit();

    }

    //	page change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {

            displayMetaInfo(position);
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };


    private void displayMetaInfo(int position) {

        TextView lblCount = (TextView) findViewById(R.id.lbl_count);
        if (imagelist_size != 0)
            lblCount.setText((position + 1) + " of " + imagelist_size);
        else
            lblCount.setText(position + " of " + imagelist_size);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.attach_file:
                Toast.makeText(this, "Select Image....", Toast.LENGTH_SHORT).show();
                Permissions permissions = new Permissions();
                if (permissions.checkPermissionREAD_EXTERNAL_STORAGE(this)) {

                    Intent chooseImageIntent = new Intent(this, CustomPhotoGalleryActivity.class);
                    startActivityForResult(chooseImageIntent, PICK_IMAGE_ID);
                }
                return true;
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == PICK_IMAGE_MULTIPLE) {
                imagesPathList = new ArrayList<>();
                imagesPath = data.getStringExtra("data").split("\\|");
            }

            for (String anImagesPath : imagesPath) {
                imagesPathList.add(anImagesPath);
            }
        }
        serviceInitforImageupdate();


    }


    public void serviceInit() {

        dataManager.getImageID(new Callback<ArrayList<String>>() {
            @Override
            public void onResponse(Call<ArrayList<String>> call, Response<ArrayList<String>> response1) {

                if (response1.isSuccessful()) {
                     user  = response1.body();

                    myViewPagerAdapter = new MyViewPagerAdapter(FreeActivity.this, user);
                    viewPager.setAdapter(myViewPagerAdapter);
                    viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
                    myViewPagerAdapter.notifyDataSetChanged();
                    imagelist_size=user.size();
                    displayMetaInfo(0);
                }
                else{
                    Toast.makeText(getApplicationContext(), " No receipts available...", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ArrayList<String>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "No receipts available... ...", Toast.LENGTH_SHORT).show();
            }

        },list_name);

    }

    public void serviceInitforImageupdate() {

        parts.clear();

        for(int i=0;i<imagesPathList.size();i++) {

            File file = new File(imagesPathList.get(i));
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part body = MultipartBody.Part.createFormData("image", file.getName(), reqFile);
            parts.add(body);
        }

        dataManager.postImage(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response2) {


                if (response2.isSuccessful()) {
                    if(myViewPagerAdapter !=null) {
                        myViewPagerAdapter.notifyDataSetChanged();
                    }
                    serviceInit();
                    Toast.makeText(getApplicationContext(), " Images uploaded", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getApplicationContext(), " Error.....   ", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(), " No response.....   ", Toast.LENGTH_SHORT).show();
            }

        }, parts,list_name);


    }

}