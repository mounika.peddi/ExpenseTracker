package com.example.jyotsna_thaninki.expensesapp;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ItemDetails extends AppCompatActivity implements OnClickListener {

    DatePickerDialog datePickerDialog;
Button save_button;
    EditText date,expense_name,amount,paid_by;
    Spinner purpose;
  SQLiteDatabase db;
    String selected;

    String es;
    String[] purposelist;
    Date myDate;
    public ItemDetails() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_item_details);
        save_button =(Button) findViewById(R.id.save);
        purposelist = getResources().getStringArray(R.array.purposelist_array);
        date=(EditText) findViewById(R.id.date_ed_txt);
        purpose=(Spinner)  findViewById(R.id.spinner_type);
        amount=(EditText) findViewById(R.id.amount_ed_txt);
        paid_by=(EditText) findViewById(R.id.paidby_ed_txt);
        expense_name=(EditText) findViewById(R.id.expense_name_ed_txt);
        save_button.setOnClickListener(this);

        date.setOnClickListener(this);


        final List<String> plantsList = new ArrayList<>(Arrays.asList(purposelist));
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,plantsList) {
            @Override
            public boolean isEnabled(int position){
                if(position == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position % 2 == 1) {
                    // Set the item background color
                    tv.setBackgroundColor(Color.parseColor("#AEB0B3"));
                } else {
                    // Set the alternate item background color
                    tv.setBackgroundColor(Color.parseColor("#E0E1E2"));
                }
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        purpose.setAdapter(spinnerArrayAdapter);

        db=openOrCreateDatabase("ExpensesDB", Context.MODE_PRIVATE, null);

        purpose.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                 selected = parent.getItemAtPosition(position).toString();
                SharedPreferences  preferences= getSharedPreferences("myprefs", Activity.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putInt("spinner_position", position);
                editor.apply();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void onClick(View view) {
        if (view == save_button) {

            if(date.getText().toString().trim().length()==0||
                    amount.getText().toString().trim().length()==0||
                    paid_by.getText().toString().trim().length()==0)
            {
                showMessage("Error", "Please enter all values");
                return;
            }
            int selectedItemOfMySpinner = purpose.getSelectedItemPosition();
            String actualPositionOfMySpinner = (String) purpose.getItemAtPosition(selectedItemOfMySpinner);

            if (actualPositionOfMySpinner.equalsIgnoreCase("Select one")) {
                setSpinnerError(purpose,"select purpose");
                return;
            }
            serviceInit();


       es=expense_name.getText().toString();
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                alertDialog.setTitle("Confirm..");
                alertDialog.setMessage("Do you want to add the receipts to expense..");
                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int which) {
                        Intent intent=new Intent(getApplicationContext(), ReceiptAttach.class);
                        intent.putExtra("expense_name",es);
                        startActivity(intent);

                    }
                });
                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
                        dialog.cancel();
                        Intent intent=new Intent(getApplicationContext(), ExpensesListActivity.class);
                        startActivity(intent);
                    }
                });
                alertDialog.show();


            }

             if (view == date) {

            // Get Current Date
             Calendar c = Calendar.getInstance();
                 c.set(Calendar.HOUR_OF_DAY, 23);
                 c.set(Calendar.MINUTE, 59);
                 c.set(Calendar.SECOND, 59);
            int  mYear = c.get(Calendar.YEAR);
            int  mMonth = c.get(Calendar.MONTH);
            int mDay = c.get(Calendar.DAY_OF_MONTH);
                 c.add(Calendar.DATE, 0);


                 datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");

                            try {
                                myDate = df.parse(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        }
                    }, mYear, mMonth, mDay);
                 datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());


                 datePickerDialog.show();
        }





    }

    private void setSpinnerError(Spinner spinner, String error){
        View selectedView = spinner.getSelectedView();
        if (selectedView != null && selectedView instanceof TextView) {
            spinner.requestFocus();
            TextView selectedTextView = (TextView) selectedView;
            selectedTextView.setWidth(50);
            selectedTextView.setError("error"); // any name of the error will do
            selectedTextView.setTextColor(Color.RED); //text color in which you want your error message to be displayed
            selectedTextView.setText(error); // actual error message

        }
    }

    public void showMessage(String title,String message)
    {
        AlertDialog.Builder builder=new AlertDialog.Builder(ItemDetails.this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }

    public void serviceInit() {

        DataManager dataManager = DataManager.getDataManager();
        SharedPreferences preferences=getSharedPreferences("myprefs", Activity.MODE_PRIVATE);

      String  mobile_number=  preferences.getString("mobile_number",null);

        dataManager.insertExpense(new Callback<ExpenselistPOJO>() {
            @Override
            public void onResponse(Call<ExpenselistPOJO> call, Response<ExpenselistPOJO> response) {
                ExpenselistPOJO name=response.body();
                System.out.print(name);

            }
            @Override
            public void onFailure(Call<ExpenselistPOJO> call, Throwable t)
            {
                Toast.makeText(getApplicationContext(), "Mobile number or password is incorrect", Toast.LENGTH_LONG).show();
            }
        },mobile_number,expense_name.getText().toString(), myDate,selected,amount.getText().toString(),paid_by.getText().toString());
    }

}
