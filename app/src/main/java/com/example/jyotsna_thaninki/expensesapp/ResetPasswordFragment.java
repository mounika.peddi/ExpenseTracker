package com.example.jyotsna_thaninki.expensesapp;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.msg91.sendotp.library.Verification;
import com.msg91.sendotp.library.VerificationListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ResetPasswordFragment extends Fragment implements VerificationListener{
    Button submit_button;
    TextView resendlink;
    EditText new_password,re_enter_password,code;
    String email_or_mobile;
    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;
    private Verification mVerification;
    public ResetPasswordFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public ResetPasswordFragment(Verification mVerification) {

        this.mVerification=mVerification;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_reset_password, container, false);
       //email_or_mobile=getIntent().getStringExtra("email_or_phone");
        resendlink=(TextView)  view.findViewById(R.id.re_send_code_tv);
        new_password=(EditText) view.findViewById(R.id.new_password_ed_txt) ;
        re_enter_password=(EditText) view.findViewById(R.id.reenterpass_ed_txt) ;
        code=(EditText) view.findViewById(R.id.code_ed_txt);
        submit_button=(Button) view.findViewById(R.id.passwordreset_submit_button);
        email_or_mobile = getArguments().getString("email_or_mobilenumber");
        code .setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!(new_password.getText().toString().equals(re_enter_password.getText().toString())))
                    Toast.makeText(getActivity(), "password mismatch please check!", Toast.LENGTH_LONG).show();

            }
        });

        submit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (new_password.getText().toString().length() == 0 || new_password.getText().toString().length() < 4 || new_password.getText().toString().length() > 12) {
                    new_password.setError("Enter password between 4-12 alphanumeric characters");
                    new_password.requestFocus();
                }
                else if(re_enter_password.getText().toString().length() == 0 || re_enter_password.getText().toString().length() < 4 || re_enter_password.getText().toString().length() > 12) {
                    re_enter_password.setError("Re-Enter password ");
                    re_enter_password.requestFocus();
                }


                else if(code.getText().toString().length() == 0 ) {
                    code.setError("Code is Required");
                    code.requestFocus();
                }
                else {
//                    String verify = code.getText().toString();
//                    mVerification.verify(verify);
//                    System.out.println(verify);

                    serviceInit();
                }
            }
        });
        return  view;
    }

    public void serviceInit() {
        // final String email=email_or_mobile;
        final String password =re_enter_password.getText().toString();
        DataManager dataManager = DataManager.getDataManager();
        UserDetails userupdatepassword=new UserDetails();
        userupdatepassword.setUser_password(password);
        dataManager.userResetPassword(new Callback<UserDetails>() {
            @Override
            public void onResponse(Call<UserDetails> call, Response<UserDetails> response) {
                UserDetails user= response.body();//User
                System.out.print(user);
                if (!(user.getError())) {
                    Fragment fragment = new LoginFragment();

                    fragmentManager = getActivity().getSupportFragmentManager();

                    fragmentTransaction = fragmentManager.beginTransaction();

                    fragmentTransaction.replace(R.id.main_container, fragment);

                    fragmentTransaction.addToBackStack(null);

                    fragmentTransaction.commit();
                    Toast.makeText(getActivity(), "password updated successfully", Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<UserDetails> call, Throwable t)
            {
                Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
            }
        },email_or_mobile,userupdatepassword);
    }

    @Override
    public void onInitiated(String response) {

    }

    @Override
    public void onInitiationFailed(Exception paramException) {

    }

    @Override
    public void onVerified(String response) {
       // System.out.println("we are in Verification code fragment  on verified method");
        serviceInit();

    }

    @Override
    public void onVerificationFailed(Exception paramException) {
       // System.out.println("we are in Verification fragment on verification failed method");
        Toast.makeText(getContext(),"something went wrong", Toast.LENGTH_SHORT).show();
    }
}
