package com.example.jyotsna_thaninki.expensesapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.msg91.sendotp.library.Verification;
import com.msg91.sendotp.library.VerificationListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmailCheckFragment extends Fragment {
    Button next_button;
    EditText email_or_mobile;
    DataManager dataManager = DataManager.getDataManager();
    String email_Or_phone;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    private Verification mVerification;
    String email_or_mobilenumber;
    public EmailCheckFragment()
    {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.activity_email_check, container, false);
        email_or_mobile=(EditText) view.findViewById(R.id.forgot_email_or_phone_edittext);
        next_button=(Button) view.findViewById(R.id.nextbutton);
        next_button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                email_Or_phone=email_or_mobile.getText().toString();
                if(email_or_mobile.getText().toString().length() == 0   )
                {

                    email_or_mobile.setError("enter mobile number");
                    email_or_mobile.requestFocus();
                }
                else if( (email_or_mobile.getText().toString().length()!=10))
                {
                    email_or_mobile.setError("not a valid number");
                    email_or_mobile.requestFocus();
                }
                else
                {
                    serviceInit();
                }

            }
        });


        return view;
    }
    public void serviceInit()
    {
        email_or_mobilenumber = email_or_mobile.getText().toString();
        DataManager dataManager = DataManager.getDataManager();
        dataManager.userCheckEmail(new Callback<UserDetails>()
        {
            @Override
            public void onResponse(Call<UserDetails> call, Response<UserDetails> response)
            {

                UserDetails user_message = response.body();
                if (!(user_message.getError()))
                {
                    //Put the value
                    ResetPasswordFragment resetPasswordFragment = new ResetPasswordFragment();
                    Bundle args = new Bundle();
                    args.putString("email_or_mobilenumber", email_or_mobilenumber);
                    resetPasswordFragment.setArguments(args);
                    fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.main_container, resetPasswordFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                    Log.e(" main ", " apt " + response.body());
                    Toast.makeText(getActivity(), "correct user....", Toast.LENGTH_LONG).show();
                }
                else
                {
                    Toast.makeText(getActivity(), "Invalid user....", Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<UserDetails> call, Throwable t)
            {
                t.printStackTrace();
                Log.e("getFS ", t.toString());
                Toast.makeText(getActivity(), "Mobile number or password is incorrect....", Toast.LENGTH_LONG).show();
            }
        },email_or_mobilenumber);
    }


}
