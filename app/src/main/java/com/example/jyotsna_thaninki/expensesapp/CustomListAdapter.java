package com.example.jyotsna_thaninki.expensesapp;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * Created by mounikapeddi on 17/2/17.
 */

public class CustomListAdapter extends ArrayAdapter
{
    private Activity context;
    private ArrayList<ExpenselistPOJO> itemlist;

    private ArrayList<ExpenselistPOJO> arraylist;


    public CustomListAdapter(Activity context, ArrayList<ExpenselistPOJO> itemlist) {
        super(context, R.layout.expenselist_name, itemlist);

        this.context = context;
        this.itemlist = itemlist;

        this.arraylist = new ArrayList<>();
        this.arraylist.addAll(itemlist);


    }

    public View getView(int position, View view, ViewGroup parent)
    {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.expenselist_name, null, true);
        TextView itemname = (TextView) rowView.findViewById(R.id.expense_name);

        itemname.setText(itemlist.get(position).getExpense_name());
        return rowView;

    }


}


