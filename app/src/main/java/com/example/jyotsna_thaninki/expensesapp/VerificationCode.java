package com.example.jyotsna_thaninki.expensesapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.msg91.sendotp.library.SendOtpVerification;
import com.msg91.sendotp.library.Verification;
import com.msg91.sendotp.library.VerificationListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class VerificationCode extends Fragment implements VerificationListener{
    Button submit;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    SpannableString resendlink;
    TextView resend;
    EditText verification_code;
    RelativeLayout relativeLayout_verification;
    String fullName,email_or_mobilenumber,Password;
    RegisterFragment Registerfragmentobject=new RegisterFragment();
    private Verification mVerification;
    @SuppressLint("ValidFragment")
    public VerificationCode(Verification mVerification) {
        this.mVerification=mVerification;
        // Required empty public constructor
    }
    public VerificationCode()
    {
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.activity_verification_code, container, false);
        fullName = getArguments().getString("fullName");
        email_or_mobilenumber= getArguments().getString("email_or_mobilenumber");
        Password = getArguments().getString("Password");
        resend=(TextView) view.findViewById(R.id.resend_verify);
        submit=(Button) view.findViewById(R.id.submit_button);
        verification_code=(EditText) view.findViewById(R.id.verification_code_ed_txt) ;
        resendlink = new SpannableString("Resend");
        resendlink.setSpan(new UnderlineSpan(), 0, resendlink.length(), 0);
        resend.setText(resendlink);
        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String country= String.valueOf(91);
                mVerification = SendOtpVerification.createSmsVerification(getActivity(), email_or_mobilenumber, VerificationCode.this, country);
                mVerification.initiate();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String verify = verification_code.getText().toString();
                mVerification.verify(verify);
                System.out.println(verify);
            }
        });
        return  view;
    }
    @Override
    public void onInitiated(String response) {
        System.out.println("we are in Verification code fragment on intitiated method");
    }
    @Override
    public void onInitiationFailed(Exception paramException) {
        System.out.println("we are in Verification code fragment  onInitiationFailed method");
        Toast.makeText(getContext(),"Entered values are invalid", Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onVerified(String response) {
        System.out.println("we are in Verification code fragment  on verified method");
        serviceInit();
    }
    @Override
    public void onVerificationFailed(Exception paramException) {
        System.out.println("we are in Verification fragment on verification failed method");
        Toast.makeText(getContext(),"we are in Verification fragment on verification failed method", Toast.LENGTH_SHORT).show();
    }
    public void serviceInit() {
        DataManager dataManager = DataManager.getDataManager();
        dataManager.userSignup(new Callback<UserDetails>() {
            @Override
            public void onResponse(Call<UserDetails> call, Response<UserDetails> response) {
                Intent intent = new Intent(getActivity(),LoginActivity.class);
                startActivity(intent);
            }
            @Override
            public void onFailure(Call<UserDetails> call, Throwable t)
            {
                Toast.makeText(getActivity(), "Mobile number or password is incorrect", Toast.LENGTH_LONG).show();
            }
        }, fullName, email_or_mobilenumber, Password);
    }
}