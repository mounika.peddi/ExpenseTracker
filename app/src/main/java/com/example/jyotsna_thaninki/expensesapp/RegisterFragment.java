package com.example.jyotsna_thaninki.expensesapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.msg91.sendotp.library.Verification;
import com.msg91.sendotp.library.VerificationListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment implements VerificationListener {
    Button createaccountbutton;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    SpannableString loginlink;
    TextView login;
    EditText email_or_phone,password,name;
    RelativeLayout relativeLayout;
    private Verification mVerification;
    public String user_name,email_or_mobilenumber,Password;
    public RegisterFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.activity_register, container, false);
        createaccountbutton=(Button) view.findViewById(R.id.create_account_button);
        email_or_phone=(EditText) view.findViewById(R.id.email_or_phone_ed_txt);
        password=(EditText) view.findViewById(R.id.password_ed_txt);
        name=(EditText) view.findViewById(R.id.name_ed_txt);

        createaccountbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (name.getText().toString().length() == 0 ) {
                    name.setError("Name Required");
                    name.requestFocus();
                }
                else if(email_or_phone.getText().toString().length() == 0   ) {

                    email_or_phone.setError("enter mobile number");
                    email_or_phone.requestFocus();
                }
                else if( (email_or_phone.getText().toString().length()!=10))
                {
                    email_or_phone.setError("not a valid number");
                    email_or_phone.requestFocus();
                }
                else if (password.getText().toString().length() == 0 || password.getText().toString().length() < 4 || password.getText().toString().length() > 12) {
                    password.setError("enter password between 4-12 alphanumeric characters");
                    password.requestFocus();
                }
                else {
                    user_name = name.getText().toString();
                    email_or_mobilenumber = email_or_phone.getText().toString();
                    Password = password.getText().toString();

                    serviceInit();
//                    email_or_mobilenumber = email_or_phone.getText().toString();
//                    final String country= String.valueOf(91);
//                    mVerification = SendOtpVerification.createSmsVerification(getActivity(), email_or_mobilenumber, RegisterFragment.this, country);
//                    mVerification.initiate();
                }
            }
        });
        login=(TextView) view.findViewById(R.id.login_link) ;
        loginlink = new SpannableString("Login");
        loginlink.setSpan(new UnderlineSpan(), 0, loginlink.length(), 0);
        login.setText(loginlink);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment=new LoginFragment();
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.main_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        return  view;
    }
    @Override
    public void onInitiated(String response) {
        user_name = name.getText().toString();
        email_or_mobilenumber = email_or_phone.getText().toString();
        Password = password.getText().toString();
        //Put the value
        VerificationCode verificationCodeFragment = new VerificationCode(mVerification);
        Bundle args = new Bundle();
        args.putString("fullName",user_name);
        args.putString("email_or_mobilenumber", email_or_mobilenumber);
        args.putString("Password", Password);
        verificationCodeFragment.setArguments(args);

        fragmentManager = getActivity().getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_container, verificationCodeFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
    @Override
    public void onInitiationFailed(Exception paramException) {
        System.out.println("we are in Registration code fragment  onInitiationFailed method");
        Toast.makeText(getContext(),"Entered values are invalid", Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onVerified(String response) {
        System.out.println("we are in Registration code fragment  on verified method");
      //  serviceInit();

    }
    @Override
    public void onVerificationFailed(Exception paramException) {
        System.out.println("we are in register fragment on verification failed method");
        Toast.makeText(getContext(),"we are in register fragment on verification failed method", Toast.LENGTH_SHORT).show();
    }

    public void serviceInit() {

        DataManager dataManager = DataManager.getDataManager();
        dataManager.userSignup(new Callback<UserDetails>() {
            @Override
            public void onResponse(Call<UserDetails> call, Response<UserDetails> response) {
                System.out.print(response.code());
                if(response.code()==200)
                {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    getActivity().startActivity(intent);
                }

            }
            @Override
            public void onFailure(Call<UserDetails> call, Throwable t)
            {
                Toast.makeText(getActivity(), "Mobile number or password is incorrect", Toast.LENGTH_LONG).show();
            }
        }, user_name, email_or_mobilenumber, Password);
    }
}