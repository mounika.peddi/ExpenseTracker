package com.example.jyotsna_thaninki.expensesapp;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by jyotsna-thaninki on 21/3/17.
 */

public interface UserDetailsAPI {
 @FormUrlEncoded
 @POST("/suraaga/1/user_login")
 Call<UserDetails> userLogin(@Field("mobile_number") String mobile_number, @Field("user_password") String user_password);

 @FormUrlEncoded
 @POST("/suraaga/1/user_register")
 Call<UserDetails> userSignup(@Field("user_name") String user_name, @Field("mobile_number") String mobile_number, @Field("user_password") String user_password);


 @PUT("/suraaga/1/user_register/{mobile_number}")
 Call<UserDetails> userResetPassword(@Path("mobile_number") String email_or_mobilenumber, @Body UserDetails body1);

 @GET("/suraaga/1/user_register_forgot/{mobile_number}")
 Call<UserDetails> userCheckEmail(@Path("mobile_number") String email_or_mobilenumber);

 @FormUrlEncoded
 @POST("/suraaga/1/myexpenselist_data/{mobile_number}")
 Call<ExpenselistPOJO> insertExpense(@Path("mobile_number") String mobile_number,@Field("expense_name") String expense_nmae, @Field("purchase_date") Date purchase_date, @Field("purpose") String purpose, @Field("amount") String amount, @Field("paid_by") String paid_by);

 @GET("/suraaga/1/singleexpenselist_data/{expense_name}")
 Call<ArrayList<ExpenselistPOJO>> getExpense(@Path("expense_name") String expense_name);


 @GET("/suraaga/1/myexpenselist_data/{mobile_number}")
 Call<ArrayList<ExpenselistPOJO>> getExpenseList(@Path("mobile_number") String mobile_number);

 @PUT("/suraaga/1/myexpenselist_data/{expense_name}")
 Call<UserDetails> updateExpense(@Path("expense_name") String expense_name, @Body ExpenselistPOJO body);

 @Multipart
 @POST("/suraaga/1/expenselist_receipt_images/{expense_name}")
 Call<ResponseBody> postImage(@Part List<MultipartBody.Part> image,@Path("expense_name") String expense_name);

 @GET("/suraaga/1/expenselist_receipt_images/{expense_name}")
 Call<ArrayList<String>> getImageID(@Path("expense_name") String expense_name);

 @DELETE("/suraaga/1/expenselist_receipt_imagesid/{id}")
 Call<ResponseBody> deleteReceipt(@Path("id") String receiptId);
}


