package com.example.jyotsna_thaninki.expensesapp;

import java.io.Serializable;

/**
 * Created by jyotsna-thaninki on 21/3/17.
 */

public class UserDetails implements Serializable{

    private String mobile_number;
    private String user_password;
    private String user_name;
    Boolean error;
    String message;


    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }
    public String getUser_password() {
        return user_password;
    }

    public void setUser_password(String user_password) {
        this.user_password = user_password;
    }
    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
