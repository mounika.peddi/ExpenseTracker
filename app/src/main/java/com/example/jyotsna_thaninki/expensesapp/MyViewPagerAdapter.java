package com.example.jyotsna_thaninki.expensesapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.File;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jyotsna-thaninki on 25/5/17.
 */

public class MyViewPagerAdapter  extends PagerAdapter {

  //  ImageView imageViewPreview;
   ImageViewTouch imageViewPreview;
    private LayoutInflater layoutInflater;
    Activity freeActivity;
   ArrayList<String> listdata;
    String url="http://192.168.91.131:3011/suraaga/1/expenselist_receipt_imagesid/";
    private ImageViewTouch.OnImageViewTouchDoubleTapListener mDoubleTapListener;
    private ImageViewTouch.OnImageViewTouchSingleTapListener mSingleTapListener;

    public MyViewPagerAdapter(FreeActivity freeActivity,ArrayList<String> listdata) {
        this.freeActivity=freeActivity;
        this.listdata=listdata;


    }


    @Override
    public int getCount() {
        return listdata.size();
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }



    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        layoutInflater = (LayoutInflater) freeActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.image_fullscreen_preview, container, false);
       imageViewPreview = (ImageViewTouch) view.findViewById(R.id.slideshow_image);

        Glide.with(freeActivity).load(url+listdata.get(position))
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageViewPreview);

        container.addView(view);
        imageViewPreview.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {


                AlertDialog.Builder alertDialog = new AlertDialog.Builder(freeActivity);
                alertDialog.setTitle("Confirm..");
                alertDialog.setMessage("Do you want to delete the receipts from expense..");
                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int which) {
                        serviceInit(position);
                        notifyDataSetChanged();
                    }
                });
                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(freeActivity, "You clicked on NO", Toast.LENGTH_SHORT).show();
                        dialog.cancel();

                    }
                });
                alertDialog.show();


                return true;
            }

        });




        imageViewPreview.setSingleTapListener(
                new ImageViewTouch.OnImageViewTouchSingleTapListener() {

                    @Override
                    public void onSingleTapConfirmed() {

                    }
                }
        );

        imageViewPreview.setDoubleTapListener(
                new ImageViewTouch.OnImageViewTouchDoubleTapListener() {

                    @Override
                    public void onDoubleTap() {

                    }
                }
        );

        return view;
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == ((View) obj);
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    public void serviceInit(final int position) {

        DataManager dataManager = DataManager.getDataManager();


        dataManager.deleteReceipt(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                if (response.isSuccessful()) {
                    listdata.remove(listdata.get(position));
                    notifyDataSetChanged();
                    Toast.makeText(freeActivity, " receipt deleted", Toast.LENGTH_LONG).show();

                }
                else {
                    Toast.makeText(freeActivity,  " Error.....   ", Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }

        },listdata.get(position) );

    }

}
