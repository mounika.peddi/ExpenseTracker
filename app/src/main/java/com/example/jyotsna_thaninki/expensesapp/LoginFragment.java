package com.example.jyotsna_thaninki.expensesapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ScrollingView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**

 * A simple {@link Fragment} subclass.

 */
public class LoginFragment extends Fragment  {
    private static final String TAG = "SignInActivity";
    private static final int RC_SIGN_IN = 9001;
    ScrollingView scrollingView;
    TextView signuplink, forgotpass_create;
    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;
    String email_or_mobilenumber,user_password;
    EditText email_or_phone, Password;
    ArrayList<UserDetails> userdetailslist;
    SpannableString content, content1;
    Button loginbutton;
    DataManager dataManager = DataManager.getDataManager();
    RelativeLayout relativeLayout;;
    private EditText usernameEditText;
    UserDetails userDetails;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        email_or_phone = (EditText) view.findViewById(R.id.input_email_or_phone_edittext);
        Password = (EditText) view.findViewById(R.id.input_password_edittext);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.login_relative_layout);
        signuplink = (TextView) view.findViewById(R.id.signup_tv);
        content = new SpannableString("Signup");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        signuplink.setText(content);
        signuplink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new RegisterFragment();
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.main_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }

        });
        forgotpass_create = (TextView) view.findViewById(R.id.forgot_createlink);
        content1 = new SpannableString("Forgot/Create Password");
        content1.setSpan(new UnderlineSpan(), 1, content1.length(), 1);
        forgotpass_create.setText(content1);
        forgotpass_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment1 = new EmailCheckFragment();
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.main_container, fragment1);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }

        });
        loginbutton = (Button) view.findViewById(R.id.login_button);
        loginbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (email_or_phone.getText().toString().length() == 0) {
                    email_or_phone.setError("enter mobile number");
                    email_or_phone.requestFocus();
                }
                else if ((email_or_phone.getText().toString().length() != 10))
                {
                    email_or_phone.setError("not a valid number");
                    email_or_phone.requestFocus();
                } else if (Password.getText().toString().length() == 0 || Password.getText().toString().length() < 4 || Password.getText().toString().length() > 12) {
                    Password.setError("enter password between 4-12 alphanumeric characters");
                    Password.requestFocus();

                } else {
                    serviceInit();
                }
            }

        });
        return view;
    }
    public void serviceInit() {
        email_or_mobilenumber = email_or_phone.getText().toString();
      user_password = Password.getText().toString();
        //UserLogin userLogin=new UserLogin(email_or_mobilenumber,password);
        dataManager.userLogin(new Callback<UserDetails>() {
            @Override
            public void onResponse(Call<UserDetails> call, Response<UserDetails> response) {
                UserDetails user = response.body();
                boolean error=user.getError();
                if(!error){

                    Intent intent=new Intent(getActivity(),ExpensesListActivity.class);
                    SharedPreferences preferences=getActivity().getSharedPreferences("myprefs", Activity.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("mobile_number", email_or_mobilenumber);
                    editor.putBoolean("isLoggedIn",true);
                    editor.apply();
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    getActivity().finish();
                    startActivity(intent);

                }
//
                 else{
                    Toast.makeText(getActivity(), "Email or password is incorrect", Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<UserDetails> call, Throwable t) {
                Toast.makeText(getActivity(), "response fail", Toast.LENGTH_LONG).show();

            }

        }, email_or_mobilenumber, user_password);


    }

    private void showMainChatActivity() {
        Intent launchIntent = new Intent();
        launchIntent.setClass(getActivity(), ExpensesListActivity.class);
        startActivity(launchIntent);
    }
}
